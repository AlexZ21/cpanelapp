#ifndef CCORE_H
#define CCORE_H

#include "ccoreglobal.h"

#include <QString>

class CCORESHARED_EXPORT CCore
{

public:
    CCore();

    static QString generateRandomString(int length);
    static QString generateRandomString();
    static QString md5(const QString &v);
};

#endif // CCORE_H
