#include "ccore.h"

#include <QDate>
#include <QTime>
#include <QCryptographicHash>

CCore::CCore()
{
}

QString CCore::generateRandomString(int length)
{
    const QString possibleCharacters("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");

    QString randomString;
    for(int i=0; i<length; ++i)
    {
        qsrand(QTime::currentTime().msec());
        int index = qrand() % possibleCharacters.length();
        QChar nextChar = possibleCharacters.at(index);
        randomString.append(nextChar);
    }
    return randomString;
}

QString CCore::generateRandomString()
{
    return md5(QString::number(QTime::currentTime().msec()) + generateRandomString(10));
}

QString CCore::md5(const QString &v)
{
    return QString(QCryptographicHash::hash(v.toLatin1(), QCryptographicHash::Md5).toHex());
}
