#ifndef CCORE_GLOBAL_H
#define CCORE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CCORE_LIBRARY)
#  define CCORESHARED_EXPORT Q_DECL_EXPORT
#else
#  define CCORESHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CCORE_GLOBAL_H
