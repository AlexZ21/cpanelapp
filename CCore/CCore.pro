#-------------------------------------------------
#
# Project created by QtCreator 2016-06-23T21:50:38
#
#-------------------------------------------------

QT       -= gui

TARGET = CCore
TEMPLATE = lib
#CONFIG += c++11

DEFINES += CCORE_LIBRARY

SOURCES += ccore.cpp

HEADERS += ccore.h \
    ccoreglobal.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
