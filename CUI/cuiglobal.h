#ifndef CUI_GLOBAL_H
#define CUI_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(CUI_LIBRARY)
#  define CUISHARED_EXPORT Q_DECL_EXPORT
#else
#  define CUISHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // CUI_GLOBAL_H
