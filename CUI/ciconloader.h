#ifndef CICONLOADER_H
#define CICONLOADER_H

#include <QObject>
#include <QImage>
#include <QMap>

class CIconTheme {
public:
    struct CIconDirectory
    {
        QString path;
        int size;
        bool scalable;
    };

    void init(const QString& searchPath, const QString& themeName);

    QImage loadIcon(const QString& iconName, int size);

    const QStringList& inheritedThemes() { return _inheritedThemes; }

private:
    bool loadIconFromDirectory(QImage& result,
                               const CIconDirectory& iconDir,
                               const QString& fileName);

private:
    QString _name;
    QString _searchPath;
    QStringList _inheritedThemes;
    QList<CIconDirectory> _iconDirs;

};

class CIconLoader : public QObject
{
    Q_OBJECT
public:
    explicit CIconLoader(QObject *parent = 0);

    static QImage loadFromTheme(const QString &themeName,
                                const QString &iconName,
                                int size);
    static QImage loadFromSystemTheme(const QString &iconName,
                                      int size);

    QImage load(const QString &iconName, int size);

    bool initTheme(const QString &themeName);
    bool initSystemTheme();

    QString themeName() const { return _themeName; }

private:
    QImage loadIconFromTheme(const QString &themeName,
                             const QString &iconName,
                             int size);

private:
    QString _themeName;
    QStringList _iconSearchPaths;

    CIconTheme _theme;
    QMap<QString, CIconTheme> _themesCache;
};

#endif // CICONLOADER_H
