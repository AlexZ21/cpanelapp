#ifndef CABOUT_H
#define CABOUT_H

#include "cuiglobal.h"
#include "cdialog.h"

class CUISHARED_EXPORT CAbout : public CDialog
{
    Q_OBJECT
public:
    CAbout(QWidget *parent = 0);
};

#endif // CABOUT_H
