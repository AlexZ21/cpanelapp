#include "ciconloader.h"

#include <stdlib.h>
#include <QIcon>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QDebug>


void CIconTheme::init(const QString &searchPath, const QString &themeName)
{
    _name = themeName;
    _searchPath = searchPath;
    QFile file(searchPath + "/" + _name + "/index.theme");

    file.open(QIODevice::ReadOnly);

    QTextStream in(&file);
    QString context;

    while(!in.atEnd())
    {
        QString line = in.readLine();

        if(line.isEmpty())
            continue;
        if(line[0] == '#')
            continue;
        if(line[0] == '[')
        {
            if(line.size() < 3)
                continue;
            context = line.mid(1, line.size() - 2);
            if(context != "Icon Theme")
            {
                CIconDirectory dir;
                dir.path = context;
                dir.size = 0;
                dir.scalable = false;
                _iconDirs.append(dir);
            }
        }

        QStringList list = line.split('=');
        if(list.size() < 2)
            continue;
        QString key = list[0];
        QString value = list[1];

        if(key == "Inherits")
            _inheritedThemes = value.split(',');

        if(key == "Size")
        {
            _iconDirs.last().size = value.toInt();
        }

        if(key == "Type")
        {
            if(value.compare("Scalable", Qt::CaseInsensitive) == 0)
                _iconDirs.last().scalable = true;
        }
    }

}

QImage CIconTheme::loadIcon(const QString &iconName, int size)
{
    QString fileNamePng = iconName;
    if(!iconName.endsWith(".png"))
        fileNamePng.append(".png");

    QString fileNameSvg = iconName;
    if(!iconName.endsWith(".svg"))
        fileNameSvg.append(".svg");

    QImage result;

    foreach(const CIconDirectory& iconDir, _iconDirs)
    {
        if(!iconDir.scalable && iconDir.size == size)
        {
            if(loadIconFromDirectory(result, iconDir, fileNamePng) ||
                    loadIconFromDirectory(result, iconDir, fileNameSvg))
                return result;
        }
    }

    return result;
}

bool CIconTheme::loadIconFromDirectory(QImage &result,
                                       const CIconTheme::CIconDirectory &iconDir,
                                       const QString &fileName)
{
    QString iconFileName = _searchPath + "/" + _name + "/" + iconDir.path + "/" + fileName;
    if(!QFile::exists(iconFileName))
        return false;
    result.load(iconFileName);
    if(!result.isNull())
        return true;
}

CIconLoader::CIconLoader(QObject *parent) : QObject(parent)
{
    _iconSearchPaths << QString(getenv("HOME")) + "/.icons";

    QString xdgDataDirs;
    char* xdgDataDirsEnv = getenv("XDG_DATA_DIRS");
    if(xdgDataDirsEnv != NULL)
        xdgDataDirs = xdgDataDirsEnv;
    else
        xdgDataDirs = "/usr/local/share/:/usr/share/";

    QStringList dirs = xdgDataDirs.split(':');
    foreach(const QString& dir, dirs)
    {
        _iconSearchPaths.append(dir + "/icons");
    }
}

QImage CIconLoader::loadFromTheme(const QString &themeName, const QString &iconName, int size)
{
    CIconLoader iconLoader;
    iconLoader.initTheme(themeName);
    return iconLoader.load(iconName, size);
}

QImage CIconLoader::loadFromSystemTheme(const QString &iconName, int size)
{
    CIconLoader iconLoader;
    iconLoader.initSystemTheme();
    return iconLoader.load(iconName, size);
}

QImage CIconLoader::load(const QString &iconName, int size)
{
    QImage result = loadIconFromTheme(_themeName, iconName, size);

    if(result.isNull())
        result.load("/usr/share/pixmaps/" + iconName);

    if(result.isNull())
        result.load(iconName);

    return result;
}

bool CIconLoader::initTheme(const QString &themeName)
{
    _themeName = themeName;

    foreach(const QString& searchPath, _iconSearchPaths)
        if(QFile::exists(searchPath + "/" + _themeName + "/index.theme"))
            return true;

    return false;
}

bool CIconLoader::initSystemTheme()
{
    return initTheme(QIcon::themeName());
}

QImage CIconLoader::loadIconFromTheme(const QString &themeName, const QString &iconName, int size)
{
    if(!_themesCache.contains(themeName)) {
        foreach(const QString& searchPath, _iconSearchPaths)
        {
            if(QFile::exists(searchPath + "/" + themeName + "/index.theme")) {
                _themesCache[themeName].init(searchPath, themeName);
                break;
            }
        }
    }

    QImage result = _themesCache[themeName].loadIcon(iconName, size);

    if(result.isNull() && themeName != "hicolor")
    {
        QStringList inheritedThemes = _themesCache[themeName].inheritedThemes();

        if(inheritedThemes.empty())
            inheritedThemes.append("hicolor");

        foreach(const QString& inheritedTheme, inheritedThemes)
        {
            result = loadIconFromTheme(inheritedTheme, iconName, size);
            if(!result.isNull())
                break;
        }
    }

    return result;
}


