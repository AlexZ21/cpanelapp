#-------------------------------------------------
#
# Project created by QtCreator 2016-06-17T17:07:54
#
#-------------------------------------------------

QT       += gui widgets

TARGET = CUI
TEMPLATE = lib
#CONFIG += c++11 console

DEFINES += CUI_LIBRARY

SOURCES += \
    cdialog.cpp \
    cabout.cpp \
    ciconloader.cpp

HEADERS += \
    cuiglobal.h \
    cdialog.h \
    cabout.h \
    ciconloader.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
