#ifndef CDIALOG_H
#define CDIALOG_H

#include "cuiglobal.h"
#include <QDialog>
#include <QPixmap>

class QLabel;

class CUISHARED_EXPORT CDialog : public QDialog
{
    Q_OBJECT
public:
    CDialog(QWidget *parent = 0);

    QWidget *workspace() const;

    QString title() const;
    void setTitle(const QString &title);

    QString description() const;
    void setDescription(const QString &description);

    void setIconVisible(bool visible);
    void setIconFromFile(const QString &iconPath);
    void setIconFromPixmap(const QPixmap &icon);

private:
    QString _title;
    QString _description;

    QWidget *_workspace;
    QLabel *_titleLabel;
    QLabel *_descriptionLabel;
    QLabel *_iconLabel;
};

#endif // CDIALOG_H
