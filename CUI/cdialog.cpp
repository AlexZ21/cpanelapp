#include "cdialog.h"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QFont>
#include <QPixmap>

#include <QDebug>

CDialog::CDialog(QWidget *parent) : QDialog(parent)
{
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->setMargin(0);
    setLayout(mainLayout);

    QWidget *headerWidget = new QWidget(this);
    headerWidget->setObjectName("headerWidget");
    headerWidget->setFixedHeight(64);
    headerWidget->setStyleSheet("#headerWidget {background: rgba(255, 255, 255, 0.5);}");

    _workspace = new QWidget(this);

    mainLayout->addWidget(headerWidget);
    mainLayout->addWidget(_workspace);

    QHBoxLayout *headerLayout1 = new QHBoxLayout();
    headerLayout1->setSpacing(10);
    headerWidget->setLayout(headerLayout1);

    _titleLabel = new QLabel(this);
    _titleLabel->hide();
    QFont f = _titleLabel->font();
    f.setPixelSize(16);
    f.setBold(true);
    _titleLabel->setFont(f);

    _descriptionLabel = new QLabel(this);
    _descriptionLabel->hide();
    _descriptionLabel->setWordWrap(true);
    _descriptionLabel->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    _iconLabel = new QLabel(this);
    _iconLabel->setFixedSize(44, 44);
    _iconLabel->hide();

    QVBoxLayout *headerLayout2 = new QVBoxLayout();
    headerLayout2->setMargin(0);
    headerLayout2->setSpacing(1);

    headerLayout2->addWidget(_titleLabel, 0, Qt::AlignVCenter);
    headerLayout2->addWidget(_descriptionLabel, 0, Qt::AlignVCenter);

    headerLayout1->addWidget(_iconLabel, 0, Qt::AlignLeft | Qt::AlignVCenter);
    headerLayout1->addLayout(headerLayout2, 1);

    resize(300, 200);
}

QWidget *CDialog::workspace() const
{
    return _workspace;
}

QString CDialog::title() const
{
    return _title;
}

void CDialog::setTitle(const QString &title)
{
    _title = title;
    _titleLabel->setVisible(!_title.isEmpty());
    _titleLabel->setText(_title);
}

QString CDialog::description() const
{
    return _description;
}

void CDialog::setDescription(const QString &description)
{
    _description = description;
    _descriptionLabel->setVisible(!_description.isEmpty());
    _descriptionLabel->setText(_description);
}

void CDialog::setIconVisible(bool visible)
{
    _iconLabel->setVisible(visible);
}

void CDialog::setIconFromFile(const QString &iconPath)
{
    QPixmap  i(iconPath);
    i = i.scaled(_iconLabel->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    _iconLabel->setPixmap(i);
    _iconLabel->show();
}

void CDialog::setIconFromPixmap(const QPixmap &icon)
{
    QPixmap i = icon;
    i = i.scaled(_iconLabel->size(), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    _iconLabel->setPixmap(i);
    _iconLabel->show();
}
