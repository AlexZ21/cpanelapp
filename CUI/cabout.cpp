#include "cabout.h"
#include "ciconloader.h"

#include <QDebug>

CAbout::CAbout(QWidget *parent) : CDialog(parent)
{
    setWindowTitle("О прогрпмме");
    setTitle("О программе");
    setIconFromPixmap(QPixmap::fromImage(CIconLoader::loadFromSystemTheme("help-about", 48)));
}
