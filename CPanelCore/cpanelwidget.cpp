#include "cpanelwidget.h"
#include "cpanelitemwidget.h"
#include "cpanel.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPainter>
#include <QStyleOption>
#include <QPen>
#include <QBrush>
#include <QFont>
#include <QFontMetrics>
#include <QEvent>
#include <QMouseEvent>
#include <QAction>
#include <QDrag>
#include <QMimeData>

#include <QLabel>
#include <QPushButton>
#include <QDebug>

CPanelWidget::CPanelWidget(CPanel *panel, QWidget *parent) :
    QWidget(parent),
    _panel(panel),
    _position(Bottom),
    _orientation(Horizontal),
    _layoutPolicy(AutoSize),
    _panelHeight(48),
    _panelWidth(300),
    _editMode(false),
    _lock(false),
    _dragging(false),
    _sticky(false)
{
    setWindowFlags(Qt::FramelessWindowHint | Qt::NoDropShadowWindowHint);
    //    setAttribute(Qt::WA_TranslucentBackground, true);
    //    setAttribute(Qt::WA_NoSystemBackground);

    setStyleSheet(".CPanelWidget {background: black;}");

    installEventFilter(this);

    _panelSettingsAction = new QAction("Параметры панели", this);
    connect(_panelSettingsAction, &QAction::triggered, [=]() {
        emit showSettings(_panel);
    });

    _aboutAction = new QAction("О программе", this);
    connect(_aboutAction, &QAction::triggered, [=]() {
        emit showAbout();
    });

}

CPanelWidget::~CPanelWidget()
{

}

CPanelWidget::Orientation CPanelWidget::orientation() const
{
    return _orientation;
}

void CPanelWidget::setOrientation(const Orientation &orientation)
{
    if (_orientation != orientation) {
        _orientation = orientation;
        updateOrientation();
        updateSize();
    }
}

void CPanelWidget::init(const QSettings &settings)
{
    // Установка настроек
    if (settings.contains("panel/height"))
        _panelHeight = settings.value("panel/height").toInt();

    if (settings.contains("panel/width"))
        _panelWidth = settings.value("panel/width").toInt();

    if (settings.contains("panel/position"))
        _position = static_cast<Position>(settings.value("panel/position").toInt());

    if (settings.contains("panel/orientation"))
        _orientation = static_cast<Orientation>(settings.value("panel/orientation").toInt());

    if (settings.contains("panel/layout_policy"))
        _layoutPolicy = static_cast<LayoutPolicy>(settings.value("panel/layout_policy").toInt());

    if (settings.contains("panel/geometry"))
        setGeometry(settings.value("panel/geometry").toRect());

    if (settings.contains("panel/lock"))
        setLock(settings.value("panel/lock").toBool());


    updateOrientation();
    updateSize();
    updatePosition();

    //    // Тестовые элементы
    //    {
    //        for (int i = 0; i < 2; ++i) {
    //            QPushButton *pb = new QPushButton(QString("Кнопка %1").arg(QString::number(i)), this);
    //            layout()->addWidget(pb);

    //            if (i == 0) {
    //                pb->setText("По вертикали");
    //                connect(pb, &QPushButton::clicked, [=](){
    //                    qDebug() << "По вертикали";
    //                    setOrientation(Vertical);
    //                });
    //            }

    //            if (i == 1) {
    //                pb->setText("По горизонтали");
    //                connect(pb, &QPushButton::clicked, [=](){
    //                    qDebug() << "По горизонтали";
    //                    setOrientation(Horizontal);
    //                });
    //            }
    //        }

    //        for (int i = 0; i < 10; ++i) {
    //            CPanelItemWidget *item = new CPanelItemWidget(this);
    //            item->setTitle(QString("Кнопка %1").arg(QString::number(i)));

    //            item->setLayout(new QVBoxLayout());

    //            QLabel *lab = new QLabel(QString("Кнопка %1").arg(QString::number(i)), item);
    //            item->layout()->addWidget(lab);

    //            item->setStyleSheet("background: red;");
    //            item->updateSize();
    //            _items << item;
    //            layout()->addWidget(item);
    //        }
    //    }
}

void CPanelWidget::sync(QSettings &settings)
{
    qInfo() << "Sync panel sttings:" << _panel->name();
    settings.setValue("panel/height", _panelHeight);
    settings.setValue("panel/width", _panelWidth);
    settings.setValue("panel/position", static_cast<int>(_position));
    settings.setValue("panel/orientation", static_cast<int>(_orientation));
    settings.setValue("panel/layout_policy", static_cast<int>(_layoutPolicy));
    settings.setValue("panel/geometry", geometry());
    settings.setValue("panel/lock", _lock);
}

CPanelWidget::LayoutPolicy CPanelWidget::layoutPolicy() const
{
    return _layoutPolicy;
}

void CPanelWidget::setLayoutPolicy(const LayoutPolicy &layoutPolicy)
{
    _layoutPolicy = layoutPolicy;
}

void CPanelWidget::updateOrientation()
{
    // Новый layout
    QLayout *l = 0;

    if (_orientation == Horizontal)
        l = new QHBoxLayout();
    else
        l = new QVBoxLayout();

    l->setSpacing(10);
    l->setMargin(0);

    // Существующий layout удаляется, а виджеты переносятся в новый
    if (layout()) {
        QLayoutItem * item;
        while ((item = layout()->takeAt(0)))
            l->addItem(item);
        delete layout();
    }

    setLayout(l);
}

void CPanelWidget::updateSize()
{
    qDebug() << "Update size";
    // Обновление размеров панели
    if (_orientation == Horizontal) {
        setMinimumHeight(_panelHeight);
        setMaximumHeight(_panelHeight);
        setMinimumWidth(_panelWidth);
        setMaximumWidth(QWIDGETSIZE_MAX);
    } else {
        setMinimumHeight(_panelWidth);
        setMaximumHeight(QWIDGETSIZE_MAX);
        setMinimumWidth(_panelHeight);
        setMaximumWidth(_panelHeight);
    }

    // Обновление размеров апплетов
    foreach (CPanelItemWidget *item, _items) {
        item->updateSize();
    }
}

QList<QAction *> CPanelWidget::panelActions()
{
    QList<QAction *> l;
    QAction *s = new QAction(this);
    s->setSeparator(true);
    l << _panelSettingsAction << s << _aboutAction;
    return l;
}

bool CPanelWidget::lock() const
{
    return _lock;
}

void CPanelWidget::setLock(bool lock)
{
    _lock = lock;
    repaint();
}

CPanelWidget::Position CPanelWidget::position() const
{
    return _position;
}

void CPanelWidget::setPosition(const Position &position)
{
    if (_position != position) {
        _position = position;
        setOrientation(_position == Left || _position == Right ? Vertical : Horizontal);
        updatePosition();
    }
}

CPanel *CPanelWidget::panel() const
{
    return _panel;
}

bool CPanelWidget::editMode() const
{
    return _editMode;
}

void CPanelWidget::setEditMode(bool editMode)
{
    _editMode = editMode;
    repaint();
}


int CPanelWidget::panelHeight() const
{
    return _panelHeight;
}

void CPanelWidget::setPanelHeight(int panelHeight)
{
    _panelHeight = panelHeight;
    updateSize();
}


int CPanelWidget::panelWidth() const
{
    return _panelWidth;
}

void CPanelWidget::setPanelWidth(int panelWidth)
{
    _panelWidth = panelWidth;
    updateSize();
}

void CPanelWidget::showPanelContextMenu(const QPoint &pos)
{
    QMenu m;
    m.addActions(panelActions());
    m.exec(pos);
}

void CPanelWidget::showItemContextMenu(CPanelItemWidget *item, const QPoint &pos)
{
    qDebug() << "Show item context menu";
}

void CPanelWidget::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e)
    QStyleOption o;
    o.initFrom(this);
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    style()->drawPrimitive(QStyle::PE_Widget, &o, &painter, this);

    // Если панель не закреплена, отображаются рамки по бокам
    if (!_lock) {
        int dotSize = 2;
        int dotSizeWithBorder = 4;

        int dotCount = (_panelHeight > dotSizeWithBorder) ?
                    (float)(_panelHeight - dotSizeWithBorder*2)/dotSizeWithBorder : 1;

        int columns = _orientation == Horizontal ? 2 : dotCount;
        int rows = _orientation == Horizontal ? dotCount : 2;

        QImage dotsImage(columns*dotSizeWithBorder,
                         rows*dotSizeWithBorder,
                         QImage::Format_ARGB32);
        dotsImage.fill(Qt::transparent);

        QPainter dotsImagePainter(&dotsImage);

        for(int i = 0; i < rows; ++i)
            for(int j = 0; j < columns; ++j)
                dotsImagePainter.fillRect(QRect(j*dotSizeWithBorder+1,
                                                i*dotSizeWithBorder+1,
                                                dotSize,
                                                dotSize), QBrush(QColor(255, 0, 0, 255)));


        dotsImagePainter.end();

        int offsetX = _orientation == Horizontal ?
                    dotSize :
                    (_panelHeight-(_orientation == Horizontal ? dotsImage.height() : dotsImage.width()))/2;

        int offsetY = _orientation == Horizontal ?
                    (_panelHeight-(_orientation == Horizontal ? dotsImage.height() : dotsImage.width()))/2 :
                    dotSize;

        painter.drawImage(offsetX, offsetY, dotsImage);

        int sOffsetX = _orientation == Horizontal ?
                    _panelWidth - dotsImage.width() - 2 :
                    (_panelHeight-(_orientation == Horizontal ? dotsImage.height() : dotsImage.width()))/2;

        int sOffsetY = _orientation == Horizontal ?
                    (_panelHeight-(_orientation == Horizontal ? dotsImage.height() : dotsImage.width()))/2 :
                    _panelWidth - dotsImage.height() - 2;

        painter.drawImage(sOffsetX, sOffsetY, dotsImage);

    }

    // Режим редактирования
    if (_editMode) {
        // Цвет рамки
        QPen p1(QColor(255, 0, 0, 155));
        p1.setWidth(4);
        painter.setPen(p1);

        // Шрифт
        QFont f;
        f.setPixelSize(9);
        painter.setFont(f);

        // Размеры названия панели
        QFontMetrics fm(f);
        int displayNameWidth = fm.width(_panel->displayName());
        int displayNameHeight = fm.height();
        QRect displayNamerect(2, 2, displayNameWidth + 4, displayNameHeight + 4);

        // Рамка
        painter.drawRect(rect());
        // Фон названия панели
        painter.fillRect(displayNamerect, QBrush(QColor(255, 0, 0, 105)));

        // Текс названия панели
        painter.setPen(Qt::white);
        painter.drawText(displayNamerect, Qt::AlignCenter, _panel->displayName());

    }
}

bool CPanelWidget::eventFilter(QObject *obj, QEvent *e)
{
    if (e->type() == QEvent::MouseButtonRelease) {
        QMouseEvent *me = static_cast<QMouseEvent *>(e);

        if (me->button() == Qt::RightButton) {
            if (dynamic_cast<CPanelWidget *>(obj) == this) {

                CPanelItemWidget *item = 0;
                foreach (CPanelItemWidget *i, _items) {
                    if (i->geometry().contains(me->pos())) {
                        item = i;
                        break;
                    }
                }

                if (item)
                    showItemContextMenu(item, QCursor::pos());
                else
                    showPanelContextMenu(QCursor::pos());

                return true;
            }
        } else if (me->button() == Qt::LeftButton) {
            if (_dragging)
                _dragging = false;
        }
    }

    if (e->type() == QEvent::MouseButtonPress) {
        QMouseEvent *me = static_cast<QMouseEvent *>(e);

        if (me->button() == Qt::LeftButton) {

            if (_orientation == Horizontal) {
                // Если панель не заблокирована, то ее можно переместить
                if (!_lock)
                    if (me->x() <= 12 || me->x() >= width()-12) {
                        _startDraggingPosition = me->globalPos();
                        _dragging = true;
                        if (_sticky)
                            _stickyPoint = me->globalPos();
                    }
            } else {
                // Если панель не заблокирована, то ее можно переместить
                if (!_lock)
                    if (me->y() <= 12 || me->y() >= height()-12) {
                        _startDraggingPosition = me->globalPos();
                        _dragging = true;
                        if (_sticky)
                            _stickyPoint = me->globalPos();
                    }
            }

        }
        return true;
    }

    if (e->type() == QEvent::MouseMove) {
        QMouseEvent *me = static_cast<QMouseEvent *>(e);

        // Перемещение панели
        if (_dragging) {
            const QPoint delta = me->globalPos() - _startDraggingPosition;
            const QRect screenFeometry = _desktopWidget.screenGeometry();
            _startDraggingPosition = me->globalPos();

            // Если горизонтальная ориентация
            if (_orientation == Horizontal) {

                if (_sticky) {
                    if (qAbs(me->globalPos().x() - _stickyPoint.x()) > 40) {
                        // fix дерганья по краям
                        if (!(x() == 0 && delta.x() < 0) &&
                                !(x() + width() == screenFeometry.width() && delta.x() > 0)) {
                            _sticky = false;
                            move(x() + (me->globalPos().x() - _stickyPoint.x()), y());
                        } else
                            _stickyPoint = me->globalPos();
                    }
                } else {
                    // Прилипание по центру
                    if ((x() + width()/2) >= screenFeometry.width()/2-20 &&
                            (x() + width()/2) <= screenFeometry.width()/2+20) {
                        _stickyPoint = me->globalPos();
                        _sticky = true;
                        move((screenFeometry.width() - width())/2, y());
                    } else if (x() >= 0 && x() <= 20) { // Прилипание к левому краю
                        _stickyPoint = me->globalPos();
                        _sticky = true;
                        move(0, y());
                    } else if (x() + width() >= screenFeometry.width() - 20 &&
                               x() + width() <= screenFeometry.width()) { // Прилипание к правому краю
                        _stickyPoint = me->globalPos();
                        _sticky = true;
                        move(screenFeometry.width() - width(), y());
                    } else {
                        if (delta.x() > 0) // Если курсор двигается вправо
                            move(x() + width() < screenFeometry.width() ? x() + delta.x() :
                                                                          screenFeometry.width() - width(), y());
                        else if (delta.x() < 0) // Если курсор двигается влево
                            move(x() > 0 ? x() + delta.x() : 0, y());
                    }
                }

            } else {
                if (_sticky) {
                    if (qAbs(me->globalPos().y() - _stickyPoint.y()) > 40) {
                        // fix дерганья по краям
                        if (!(y() == 0 && delta.y() < 0) &&
                                !(y() + height() == screenFeometry.height() && delta.y() > 0)) {
                            _sticky = false;
                            move(x(), y() + (me->globalPos().y() - _stickyPoint.y()));
                        } else
                            _stickyPoint = me->globalPos();
                    }
                } else {
                    // Прилипание по центру
                    if ((y() + height()/2) >= screenFeometry.height()/2-20 &&
                            (y() + height()/2) <= screenFeometry.height()/2+20) {
                        _stickyPoint = me->globalPos();
                        _sticky = true;
                        move(x(), (screenFeometry.height() - height())/2);
                    } else if (y() >= 0 && y() <= 20) { // Прилипание к верхнему краю
                        _stickyPoint = me->globalPos();
                        _sticky = true;
                        move(x(), 0);
                    } else if (y() + height() >= screenFeometry.height() - 20 &&
                               y() + height() <= screenFeometry.height()) { // Прилипание к нижнему краю
                        _stickyPoint = me->globalPos();
                        _sticky = true;
                        move(x(), screenFeometry.height() - height());
                    } else {
                        if (delta.y() > 0) // Если курсор двигается вправо
                            move(x(), y() + height() < screenFeometry.height() ? y() + delta.y() :
                                                                                 screenFeometry.height() - height());
                        else if (delta.y() < 0) // Если курсор двигается влево
                            move(x(), y() > 0 ? y() + delta.y() : 0);
                    }
                }

            }
        }
        return true;
    }

    return QObject::eventFilter(obj, e);
}

void CPanelWidget::updatePosition()
{
    QRect screenGeometry = _desktopWidget.screenGeometry();

    if (_position == Top) {
        move(0, 0);
    } else if (_position == Bottom) {
        move(0, screenGeometry.height() - height());
    } else if (_position == Left) {
        move(0, 0);
    } else if (_position == Right) {
        move(screenGeometry.width() - width(), 0);
    }
}
