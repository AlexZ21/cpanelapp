#include "cpanelapplet.h"

CPanelApplet::CPanelApplet(QObject *parent) : QObject(parent)
{

}

QString CPanelApplet::name() const
{
    return _name;
}

void CPanelApplet::setName(const QString &name)
{
    _name = name;
}

QString CPanelApplet::description() const
{
    return _description;
}

void CPanelApplet::setDescription(const QString &description)
{
    _description = description;
}

QString CPanelApplet::version() const
{
    return _version;
}

void CPanelApplet::setVersion(const QString &version)
{
    _version = version;
}
