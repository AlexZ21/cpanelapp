#ifndef CPANEL_H
#define CPANEL_H

#include <cpanelcore.h>
#include <cpanelwidget.h>

#include <QString>

class CPANELCORESHARED_EXPORT CPanel
{
public:
    CPanel();
    CPanel(const QString &path);
    ~CPanel();

    // Инициализация панели
    void init();

    // Путь к папке с конфигами панели и ее аплетами
    QString path() const;
    void setPath(const QString &path);

    // Имя панели
    QString name() const;
    void setName(const QString &name);

    //! Отображаемое имя панели
    QString displayName() const;
    void setDisplayName(const QString &displayName);

    //! Сохранить конфиги панели
    bool save();

    //! Удалить конфиги панели
    bool remove();

    CPanelWidget *panelWidget() const;

private:
    QString _path;
    QString _name;
    QString _displayName;

    CPanelWidget *_panelWidget;
};

#endif // CPANEL_H
