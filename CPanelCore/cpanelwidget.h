#ifndef CPANELWIDGET_H
#define CPANELWIDGET_H

#include <cpanelcore.h>

#include <QWidget>
#include <QSettings>
#include <QDesktopWidget>
#include <QMenu>

class QAction;
class CPanel;
class CPanelItemWidget;

class CPANELCORESHARED_EXPORT CPanelWidget : public QWidget
{
    Q_OBJECT
public:
    //! Положение на экране
    enum Position {
        Left = 0,
        Top = 1,
        Right = 2,
        Bottom = 3
    };

    //! Ориентация
    enum Orientation
    {
        Horizontal = 0,
        Vertical = 1,
    };

    enum LayoutPolicy
    {
        Normal,
        AutoSize,
        FillSpace,
    };

public:
    explicit CPanelWidget(CPanel *panel, QWidget *parent = 0);
    ~CPanelWidget();

    //! Инициализация панели
    void init(const QSettings &settings);

    //! Синхронизированть настройки панели
    void sync(QSettings &settings);

    //! Ориентация
    Orientation orientation() const;
    void setOrientation(const Orientation &orientation);

    LayoutPolicy layoutPolicy() const;
    void setLayoutPolicy(const LayoutPolicy &layoutPolicy);

    //! Высота панели
    int panelHeight() const;
    void setPanelHeight(int panelHeight);

    //! Ширина панели
    int panelWidth() const;
    void setPanelWidth(int panelWidth);

    //! Контекстное меню панели
    void showPanelContextMenu(const QPoint &pos);
    void showItemContextMenu(CPanelItemWidget *item, const QPoint &pos);

    //! Режим редактирования
    bool editMode() const;
    void setEditMode(bool editMode);

    CPanel *panel() const;

    //! Положение на экране
    Position position() const;
    void setPosition(const Position &position);

    bool lock() const;
    void setLock(bool lock);

protected:
    void paintEvent(QPaintEvent *e);
    bool eventFilter(QObject *obj, QEvent *e);

private:
    //! Обновить положение на экране
    void updatePosition();
    void updateOrientation();
    void updateSize();
    QList<QAction*> panelActions();

signals:
    void showSettings(CPanel *panel);
    void showAbout();

public slots:

private:
    CPanel *_panel;

    Position _position; //!< Положение панели на экране
    Orientation _orientation; //!< Ориентация панели (вертикальная/горизонтальная)

    LayoutPolicy _layoutPolicy;

    int _panelHeight;
    int _panelWidth;

    bool _editMode;
    bool _lock;

    QAction *_panelSettingsAction;
    QAction *_aboutAction;

    QList<CPanelItemWidget*> _items;

    QDesktopWidget _desktopWidget; //!< Определение размера экрана и т.д.

    // Пересещение панели
    QPoint _startDraggingPosition; //!< Координата с которой перетаскивается панель
    bool _dragging; //!< Находится ли панель в состоянии перетаскивания
    bool _sticky; //!< Прилипание панели
    QPoint _stickyPoint; //< Координата для определения состояния прилипания
};

#endif // CPANELWIDGET_H
