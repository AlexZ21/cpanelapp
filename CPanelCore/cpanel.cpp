#include "cpanel.h"

#include <QSettings>
#include <QDir>
#include <QDebug>

CPanel::CPanel() : _panelWidget(nullptr)
{

}

CPanel::CPanel(const QString &path) : _panelWidget(nullptr)
{
    setPath(path);
}

CPanel::~CPanel()
{
    delete _panelWidget;
}

void CPanel::init()
{
    // Инициализация конфига панели
    if (!_path.isEmpty()) {
        QSettings settings(_path + "panel.conf", QSettings::NativeFormat);

        // Установка полей из конфига, если панель не новая
        if (settings.contains("name")) {
            setName(settings.value("name").toString());
            setDisplayName(settings.value("display_name").toString());
        }

        _panelWidget = new CPanelWidget(this);
        _panelWidget->init(settings);
        _panelWidget->setWindowTitle(_displayName);
    }
}

QString CPanel::path() const
{
    return _path;
}

void CPanel::setPath(const QString &path)
{
    _path = path;
    if (!_path.endsWith("/"))
        _path += "/";
}

QString CPanel::name() const
{
    return _name;
}

void CPanel::setName(const QString &name)
{
    _name = name;
}


QString CPanel::displayName() const
{
    return _displayName;
}

void CPanel::setDisplayName(const QString &displayName)
{
    _displayName = displayName;
    if (_panelWidget)
        _panelWidget->setWindowTitle(_displayName);
}

bool CPanel::save()
{
    QSettings settings(_path + "panel.conf", QSettings::NativeFormat);

    settings.setValue("name", _name);
    settings.setValue("display_name", _displayName);

    _panelWidget->sync(settings);

    settings.sync();

    return settings.status() != QSettings::NoError;
}

bool CPanel::remove()
{
    QDir d(_path);
    return d.removeRecursively();
}

CPanelWidget *CPanel::panelWidget() const
{
    return _panelWidget;
}
