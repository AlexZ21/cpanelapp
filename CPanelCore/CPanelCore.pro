#-------------------------------------------------
#
# Project created by QtCreator 2016-06-16T17:27:55
#
#-------------------------------------------------

QT       += widgets

TARGET = CPanelCore
TEMPLATE = lib
#CONFIG += c++11 console

DEFINES += CPANELCORE_LIBRARY

SOURCES += \
    cpanelwidget.cpp \
    cpanelitemwidget.cpp \
    cpanelapplet.cpp \
    cpanel.cpp

HEADERS +=\
    cpanelwidget.h \
    cpanelitemwidget.h \
    cpanelcore.h \
    cpanelapplet.h \
    cpanel.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}
