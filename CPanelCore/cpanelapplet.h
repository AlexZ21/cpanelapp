#ifndef CPANELAPPLET_H
#define CPANELAPPLET_H

#include <QObject>
#include <cpanelcore.h>

class CPANELCORESHARED_EXPORT CPanelApplet : public QObject
{
    Q_OBJECT
public:
    explicit CPanelApplet(QObject *parent = 0);

    QString name() const;
    void setName(const QString &name);

    QString description() const;
    void setDescription(const QString &description);

    QString version() const;
    void setVersion(const QString &version);

signals:

public slots:

private:
    QString _name;
    QString _description;
    QString _version;
};

#endif // CPANELAPPLET_H
