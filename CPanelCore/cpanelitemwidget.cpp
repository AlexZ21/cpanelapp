#include "cpanelitemwidget.h"

#include <QPainter>
#include <QStyleOption>

CPanelItemWidget::CPanelItemWidget(CPanelWidget *panel, QWidget *parent) :
    QWidget(parent),
    _panel(panel),
    _itemHeight(panel->panelHeight()),
    _itemWidth(50)
{
}

void CPanelItemWidget::updateSize()
{
    if (_panel->orientation() == CPanelWidget::Horizontal) {
        setMinimumHeight(_itemHeight);
        setMaximumHeight(_itemHeight);
        setMinimumWidth(_itemWidth);
        setMaximumWidth(QWIDGETSIZE_MAX);
    } else {
        setMinimumHeight(_itemWidth);
        setMaximumHeight(QWIDGETSIZE_MAX);
        setMinimumWidth(_itemHeight);
        setMaximumWidth(_itemHeight);
    }
}

int CPanelItemWidget::itemHeight() const
{
    return _itemHeight;
}

void CPanelItemWidget::setItemHeight(int itemHeight)
{
    _itemHeight = itemHeight;
}

int CPanelItemWidget::itemWidth() const
{
    return _itemWidth;
}

void CPanelItemWidget::setItemWidth(int itemWidth)
{
    _itemWidth = itemWidth;
}

void CPanelItemWidget::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e)
    QStyleOption o;
    o.initFrom(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &o, &painter, this);
}

QString CPanelItemWidget::title() const
{
    return _title;
}

void CPanelItemWidget::setTitle(const QString &title)
{
    _title = title;
}
