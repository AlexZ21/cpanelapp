#ifndef CPANELITEMWIDGET_H
#define CPANELITEMWIDGET_H

#include <QWidget>
#include <cpanelcore.h>
#include <cpanelwidget.h>

class CPANELCORESHARED_EXPORT CPanelItemWidget : public QWidget
{
    Q_OBJECT
public:
    explicit CPanelItemWidget(CPanelWidget *panel, QWidget *parent = 0);

    void updateSize();

    int itemHeight() const;
    void setItemHeight(int itemHeight);

    int itemWidth() const;
    void setItemWidth(int itemWidth);

    QString title() const;
    void setTitle(const QString &title);

protected:
    void paintEvent(QPaintEvent *e);

signals:

public slots:

private:
    CPanelWidget *_panel;
    int _itemHeight;
    int _itemWidth;
    QString _title;
};

#endif // CPANELITEMWIDGET_H
