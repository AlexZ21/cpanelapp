#include "cpanelsettingsdialog.h"
#include "ui_panelsettingsform.h"

#include <cpanelapplet.h>
#include <cpanelwidget.h>
#include <cpanel.h>
#include <ciconloader.h>

#include <QStandardItemModel>
#include <QStandardItem>
#include <QIcon>
#include <QToolButton>
#include <QComboBox>
#include <QDebug>

CPanelSettingsDialog::CPanelSettingsDialog(CPanelApplication *app) : CDialog(),
    _ui(new Ui::PanelSettingsForm),
    _app(app),
    _currentPanel(nullptr)
{
    setWindowModality(Qt::WindowModal);

    _ui->setupUi(workspace());

    CIconLoader il;
    il.initSystemTheme();

    setWindowTitle(tr("Settings"));
    setTitle(tr("Panel settings"));
    setDescription(tr("Controlling the appearance of the panels and applets"));
    setIconFromPixmap(QPixmap::fromImage(il.load("preferences-system", 48)));

    // Высота комбобокса с панелями
    _ui->panelsComboBox->setFixedHeight(34);

    _ui->addPanelButton->setIcon(QIcon(QPixmap::fromImage(il.load("add", 24))));
    _ui->removePanelButton->setIcon(QIcon(QPixmap::fromImage(il.load("remove", 24))));

    // Добавить панель
    connect(_ui->addPanelButton, &QToolButton::clicked, [=](){
        CPanel *p = _app->panelManeger().createPanel();

        connect(p->panelWidget(), &CPanelWidget::showSettings,
                _app, &CPanelApplication::showSettingsDialog);
        connect(p->panelWidget(), &CPanelWidget::showAbout,
                _app, &CPanelApplication::showAboutDialog);

        p->panelWidget()->show();

        QStandardItem *item = new QStandardItem();
        item->setData(p->name(), PANEL_NAME_ROLE);
        _panelsModel->appendRow(item);

        _ui->panelsComboBox->setCurrentIndex(_panelsModel->rowCount() - 1);

        updateCurrentPanel();
        updatePanelTitles();
    });

    // Удалить панель
    connect(_ui->removePanelButton, &QToolButton::clicked, [=](){
        if (_panelsModel->rowCount() > 1) {
            if (_app->panelManeger().
                    removePanel(_ui->panelsComboBox->currentData(PANEL_NAME_ROLE).toString())) {
                _panelsModel->removeRow(_ui->panelsComboBox->currentIndex());

                _ui->panelsComboBox->setCurrentIndex(_panelsModel->rowCount() - 1);

                _currentPanel = nullptr;
                updateCurrentPanel();
                updatePanelTitles();
            }
        }
    });

    // Сменилась панель
    connect(_ui->panelsComboBox,
            static_cast<void (QComboBox::*)(int)>(&QComboBox::activated), [=](int index){
        updateCurrentPanel();
    });

    _ui->panelPosition->addItem(tr("Left"));
    _ui->panelPosition->addItem(tr("Top"));
    _ui->panelPosition->addItem(tr("Right"));
    _ui->panelPosition->addItem(tr("Bottom"));

    // Положение панели
    connect(_ui->panelPosition,
            static_cast<void (QComboBox::*)(int)>(&QComboBox::activated), [=](int index){
        if (_currentPanel) {
            _currentPanel->panelWidget()->setPosition(static_cast<CPanelWidget::Position>(index));
        }
    });

    // Блокировка панели
    connect(_ui->lock, &QCheckBox::stateChanged, [=](bool state){
        if (_currentPanel) {
            _currentPanel->panelWidget()->setLock(state);
        }
    });

    // Высота панели
    connect(_ui->panelHeight,
            static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [=](int value) {
        if (_currentPanel) {
            _currentPanel->panelWidget()->setPanelHeight(value);
        }
    });

    // Автоматическая ширина
    connect(_ui->autoWidth, &QCheckBox::toggled, [=](bool state){
        _ui->panelWidth->setEnabled(!state);
    });

    // Длина панели
    connect(_ui->panelWidth,
            static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), [=](int value) {
        if (_currentPanel) {
            _currentPanel->panelWidget()->setPanelWidth(value);
        }
    });

    // Список панелей
    _panelsModel = new QStandardItemModel(this);
    int n = 1;
    foreach (CPanel *p, _app->panelManeger().panels()) {
        QStandardItem *item = new QStandardItem();
        item->setData(p->name(), PANEL_NAME_ROLE);
        p->setDisplayName(QString(tr("Panel ")) + QString::number(n++));
        item->setData(p->displayName(), Qt::DisplayRole);
        _panelsModel->appendRow(item);
    }

    _ui->panelsComboBox->setModel(_panelsModel);

    updateCurrentPanel();
}

CPanelSettingsDialog::~CPanelSettingsDialog()
{
    // Сохранение изменений для панелей
    _app->panelManeger().saveAll();
    if (_currentPanel)
        _currentPanel->panelWidget()->setEditMode(false);
    delete _ui;
}

void CPanelSettingsDialog::setCurrentPanel(const QString &panelName)
{
    QModelIndexList l = _panelsModel->match(_panelsModel->index(0, 0),
                                            PANEL_NAME_ROLE,
                                            panelName,
                                            1,
                                            Qt::MatchFixedString);
    if (!l.isEmpty()) {
        _ui->panelsComboBox->setCurrentIndex(l.value(0).row());
        updateCurrentPanel();
    }
}

void CPanelSettingsDialog::updateForms()
{
    if (_currentPanel) {
        _ui->panelPosition->setCurrentIndex(static_cast<int>(_currentPanel->panelWidget()->position()));
        _ui->panelHeight->setValue(_currentPanel->panelWidget()->panelHeight());
        _ui->panelWidth->setValue(_currentPanel->panelWidget()->panelWidth());
        _ui->lock->setChecked(_currentPanel->panelWidget()->lock());
    }
}

void CPanelSettingsDialog::updateCurrentPanel()
{
    if (_currentPanel)
        _currentPanel->panelWidget()->setEditMode(false);
    _currentPanel = _app->panelManeger().
            panel(_ui->panelsComboBox->currentData(PANEL_NAME_ROLE).toString());
    _currentPanel->panelWidget()->setEditMode(true);
    updateForms();
}

void CPanelSettingsDialog::updatePanelTitles()
{
    // Обновим названия панелей
    QModelIndex i = _panelsModel->index(0, 0);
    while (i.isValid()) {
        CPanel *p = _app->panelManeger().panel(i.data(PANEL_NAME_ROLE).toString());
        p->setDisplayName(QString(tr("Panel ")) + QString::number(i.row() + 1));
        _panelsModel->setData(i, p->displayName(), Qt::DisplayRole);
        i = i.sibling(i.row() + 1, 0);
    }
}
