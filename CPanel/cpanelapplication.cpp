#include "cpanelapplication.h"
#include "cpanelsettingsdialog.h"
#include "cpanelmanager.h"

#include <cpanelwidget.h>
#include <cpanelapplet.h>
#include <cpanel.h>
#include <cabout.h>
#include <ccore.h>

#include <QSettings>
#include <QStandardPaths>
#include <QDir>
#include <QPluginLoader>
#include <QDebug>

CPanelApplication::CPanelApplication(int &argc, char **argv) :
    QApplication(argc, argv),
    _settings(0)
{
    QApplication::setOrganizationDomain("alexcrn.com");
    QApplication::setApplicationName("CPanel");
    QApplication::setOrganizationName("AlexCrn");
    _settings = new QSettings(organizationName(), applicationName());
}

CPanelApplication::~CPanelApplication()
{
    qDeleteAll(_panels);
    qDeleteAll(_applets);
    delete _settings;
}

void CPanelApplication::setCustomDataPath(const QString &dataPath)
{
    _dataPath = dataPath;
}

void CPanelApplication::init()
{
    initPaths();

    _panelManeger.setPanelsDirPath(_dataPath + "/panels/");
    _panelManeger.loadPanels();

    if (_panelManeger.isEmpty()) {
        _panelManeger.createPanel();
    }

    foreach (const QString &panelname, _panelManeger.panelsMap().keys()) {
        CPanelWidget *p = _panelManeger.panel(panelname)->panelWidget();

        connect(p, &CPanelWidget::showSettings,
                this, &CPanelApplication::showSettingsDialog);
        connect(p, &CPanelWidget::showAbout,
                this, &CPanelApplication::showAboutDialog);

        p->show();
    }

    //    loadApplets();
}

void CPanelApplication::showSettingsDialog(CPanel *panel)
{
    CPanelSettingsDialog panelSettingDialog(this);
    panelSettingDialog.setCurrentPanel(panel->name());
    panelSettingDialog.exec();
}

void CPanelApplication::showAboutDialog()
{
    CAbout aboutDialog;
    aboutDialog.exec();
}

void CPanelApplication::initPaths()
{
    if (_dataPath.isEmpty())
        _dataPath = QStandardPaths::writableLocation(QStandardPaths::DataLocation);

    QDir dataDir(_dataPath);
    if (!dataDir.exists())
        dataDir.mkpath(".");
}

void CPanelApplication::loadApplets()
{
    // Загрузка апплетов
    QDir appletsDir(_dataPath + "/applets/");
    if (!appletsDir.exists())
        appletsDir.mkpath(".");
    appletsDir.setNameFilters(QStringList() << "*.so*");
    QStringList appletPaths = appletsDir.entryList();

    if (!appletPaths.isEmpty()) {
        foreach (QString appletPath, appletPaths) {
            QPluginLoader loader;
            loader.setFileName(appletsDir.path() + "/" + appletPath);
            CPanelApplet *applet = qobject_cast<CPanelApplet *>(loader.instance());
            if (applet) {
                if (!_applets.contains(applet->name()))
                    _applets.insert(applet->name(), applet);
            }
        }
    }
}

CPanelManager &CPanelApplication::panelManeger()
{
    return _panelManeger;
}

QMap<QString, CPanelApplet *> CPanelApplication::applets()
{
    return _applets;
}
