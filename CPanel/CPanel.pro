#-------------------------------------------------
#
# Project created by QtCreator 2016-06-16T13:39:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CPanel
TEMPLATE = app
#CONFIG += c++11 console

SOURCES += main.cpp \
    cpanelapplication.cpp \
    cpanelsettingsdialog.cpp \
    cpanelmanager.cpp

HEADERS  += \
    cpanelapplication.h \
    cpanelsettingsdialog.h \
    cpanelmanager.h

RESOURCES += \
    cpanel.qrc

FORMS += \
    forms/panelsettingsform.ui

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../CPanelCore/release/ -lCPanelCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../CPanelCore/debug/ -lCPanelCore
else:unix: LIBS += -L$$OUT_PWD/../CPanelCore/ -lCPanelCore

INCLUDEPATH += $$PWD/../CPanelCore
DEPENDPATH += $$PWD/../CPanelCore

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../CUI/release/ -lCUI
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../CUI/debug/ -lCUI
else:unix: LIBS += -L$$OUT_PWD/../CUI/ -lCUI

INCLUDEPATH += $$PWD/../CUI
DEPENDPATH += $$PWD/../CUI

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../CCore/release/ -lCCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../CCore/debug/ -lCCore
else:unix: LIBS += -L$$OUT_PWD/../CCore/ -lCCore

INCLUDEPATH += $$PWD/../CCore
DEPENDPATH += $$PWD/../CCore
