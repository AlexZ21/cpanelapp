#include <cpanelapplication.h>

int main(int argc, char *argv[])
{
    CPanelApplication a(argc, argv);
    a.init();
    return a.exec();
}
