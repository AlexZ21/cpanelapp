#include "cpanelmanager.h"

#include <ccore.h>
#include <cpanel.h>

#include <QDir>
#include <QFile>
#include <QDebug>

CPanelManager::CPanelManager()
{

}

CPanelManager::~CPanelManager()
{
    saveAll();
    qDeleteAll(_panels);
}

void CPanelManager::loadPanels()
{
    if (!_panelsDirPath.isEmpty()) {
        // Загрузка панелей
        QDir panelsDir(_panelsDirPath);
        if (panelsDir.exists()) {
            QStringList panelsDirPaths = panelsDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
            foreach (QString panelDirName, panelsDirPaths) {
                loadPanel(panelDirName);
            }
        }
    }
}

CPanel *CPanelManager::loadPanel(const QString &panelDirName)
{
    if (QFile::exists(_panelsDirPath + panelDirName + "/panel.conf")) {
        CPanel *p = new CPanel(_panelsDirPath + panelDirName);
        p->init();
        p->setName(panelDirName);
        p->setDisplayName(QString(tr("Panel ")) + QString::number(count() + 1));
        _panels.insert(panelDirName, p);
        qInfo() << "Load panel:" << panelDirName;
        return p;
    }
    return nullptr;
}

CPanel *CPanelManager::createPanel()
{
    QString panelName = CCore::generateRandomString();
    CPanel *p = new CPanel(_panelsDirPath + panelName);
    p->init();
    p->setName(panelName);
    p->setDisplayName(QString(tr("Panel ")) + QString::number(count() + 1));
    _panels.insert(panelName, p);
    qInfo() << "Create panel:" << panelName;
    return p;
}

bool CPanelManager::removePanel(const QString &panelName)
{
    CPanel *p = panel(panelName);
    if (p->remove()) {
        delete p;
        p = nullptr;
        _panels.remove(panelName);
        qInfo() << "Remove panel:" << panelName;
        return true;
    }
    return false;
}

CPanel *CPanelManager::panel(const QString &panelName)
{
    return _panels.contains(panelName) ? _panels[panelName] : nullptr;
}

QList<CPanel *> CPanelManager::panels()
{
    return _panels.values();
}

const QMap<QString, CPanel *> &CPanelManager::panelsMap() const
{
    return _panels;
}

void CPanelManager::saveAll()
{
    foreach (const QString &panelName, _panels.keys())
        panel(panelName)->save();
}

QString CPanelManager::panelsDirPath() const
{
    return _panelsDirPath;
}

void CPanelManager::setPanelsDirPath(const QString &panelDirsPath)
{
    _panelsDirPath = panelDirsPath;

    if (!_panelsDirPath.endsWith("/"))
        _panelsDirPath += "/";
}
