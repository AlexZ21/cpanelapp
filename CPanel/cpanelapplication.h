#ifndef CPANELAPPLICATION_H
#define CPANELAPPLICATION_H

#include <QApplication>
#include "cpanelmanager.h"

class QSettings;
class CPanel;
class CPanelWidget;
class CPanelApplet;

class CPanelApplication : public QApplication
{
    Q_OBJECT
public:
    CPanelApplication(int &argc, char **argv);
    ~CPanelApplication();

    //! Установка пользовательского пути к папке с данными
    //! Если не установлена получается автоматически
    void setCustomDataPath(const QString &dataPath);

    //! Инициализация приложения
    void init();

    QMap<QString, CPanelApplet *> applets();

    CPanelManager &panelManeger();

public slots:
    void showSettingsDialog(CPanel *panel);
    void showAboutDialog();

private:
    //! Установка путей
    void initPaths();

    //! Загрузка панелей и их конфигураций
    void loadPanelsConfigs();

    //! Загрузка апплетов
    void loadApplets();

private:
    QSettings *_settings; //!< Параметры приложения
    QString _dataPath; //! Путь к папке с данными
    QMap<QString, CPanelWidget *> _panels; //! Панели
    QMap<QString, CPanelApplet *> _applets; //! Апплеты
    CPanelManager _panelManeger;
};

#endif // CPANELAPPLICATION_H
