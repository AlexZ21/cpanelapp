#ifndef CPANELSETTINGSDIALOG_H
#define CPANELSETTINGSDIALOG_H

#define PANEL_NAME_ROLE             Qt::UserRole+110

#include <cdialog.h>

#include "cpanelapplication.h"

class QStandardItemModel;
class CPanel;

namespace Ui {
    class PanelSettingsForm;
}

class CPanelSettingsDialog : public CDialog
{
    Q_OBJECT
public:
    CPanelSettingsDialog(CPanelApplication *app);
    ~CPanelSettingsDialog();

    void setCurrentPanel(const QString &panelName);

private:
    void updateForms();
    void updateCurrentPanel();
    void updatePanelTitles();

private:
    Ui::PanelSettingsForm *_ui;
    CPanelApplication *_app;
    CPanel *_currentPanel;
    QStandardItemModel *_panelsModel;
};

#endif // CPANELSETTINGSDIALOG_H
