#ifndef CPANELMANAGER_H
#define CPANELMANAGER_H

#include <QCoreApplication>
#include <QString>
#include <QMap>

class CPanel;

class CPanelManager
{
    Q_DECLARE_TR_FUNCTIONS(CPanelManager)
public:
    CPanelManager();
    ~CPanelManager();

    //! Загрузить панели
    void loadPanels();
    //! Загрузить панель
    CPanel *loadPanel(const QString &panelDirName);

    //! Добавить новую панель
    CPanel *createPanel();
    //! Удалить панель
    bool removePanel(const QString &panelName);

    //! Получить панель по ее имени
    CPanel *panel(const QString &panelName);
    //! Получить список панелей
    QList<CPanel *> panels();
    //! Получить map панелей
    const QMap<QString, CPanel *> &panelsMap() const;

    //! Проверка на пустоту контейнера панелей
    int isEmpty() { return _panels.isEmpty(); }
    //! Количество панелей
    int count() { return _panels.count(); }

    void saveAll();

    //! Корневая деректория панелей
    QString panelsDirPath() const;
    void setPanelsDirPath(const QString &panelsDirPath);

private:
    QString _panelsDirPath;

    QMap<QString, CPanel*> _panels;
};

#endif // CPANELMANAGER_H
