#include "cpaneltestapplet.h"


CPanelTestApplet::CPanelTestApplet(QObject *parent) :
    CPanelApplet(parent)
{
    setName("CPanelTestApplet");
    setDescription("Test applet for cpanel");
    setVersion("1.0");
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(CPanelTestApplet, CPanelTestApplet)
#endif // QT_VERSION < 0x050000
