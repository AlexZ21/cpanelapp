#-------------------------------------------------
#
# Project created by QtCreator 2016-06-17T16:38:47
#
#-------------------------------------------------

QT       += core gui

TARGET = CPanelTestApplet
TEMPLATE = lib
#CONFIG += c++11

DEFINES += CPANELTESTAPPLET_LIBRARY

SOURCES += cpaneltestapplet.cpp

HEADERS += cpaneltestapplet.h \
    cpaneltestappletglobal.h
DISTFILES +=

unix {
    target.path = /usr/lib
    INSTALLS += target
}

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../CPanelCore/release/ -lCPanelCore
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../CPanelCore/debug/ -lCPanelCore
else:unix: LIBS += -L$$OUT_PWD/../../CPanelCore/ -lCPanelCore

INCLUDEPATH += $$PWD/../../CPanelCore
DEPENDPATH += $$PWD/../../CPanelCore
