#ifndef CPANELTESTAPPLET_H
#define CPANELTESTAPPLET_H

#include <QtPlugin>
#include "cpaneltestappletglobal.h"
#include <cpanelapplet.h>

class CPANELTESTAPPLET_EXPORT CPanelTestApplet : public CPanelApplet
{
    Q_OBJECT
#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "CPanelTestApplet")
#endif // QT_VERSION >= 0x050000

public:
    CPanelTestApplet(QObject *parent = 0);
};

#endif // CPANELTESTAPPLET_H
